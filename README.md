# mu_calculator
## Description
Calculate power from a rolling friction coefficient or the other way around for the Esseda.

## Screenshot
![screenshot](screenshot.png)

## Authors
* **Rick de Klerk** - *Initial work* - [gitlab](https://gitlab.com/rickdkk) - [UMCG](https://www.rug.nl/staff/r.de.klerk/)
* **Thomas Rietveld** - *Initial work* - [UMCG](https://www.rug.nl/staff/t.rietveld/)


## License
This project is licensed under the GNU GPLv3 - see the [LICENSE](LICENSE) file for details.
