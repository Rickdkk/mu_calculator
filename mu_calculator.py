import sys

from PySide2 import QtCore, QtWidgets
from PySide2.QtGui import QColor


class Ui_Dialog(object):  # automatically generated UI
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(250, 152)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        self.verticalLayout = QtWidgets.QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setObjectName("formLayout")
        self.weight_pp_Label = QtWidgets.QLabel(Dialog)
        self.weight_pp_Label.setObjectName("weight_pp_Label")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.weight_pp_Label)
        self.weight_pp_Box = QtWidgets.QDoubleSpinBox(Dialog)
        self.weight_pp_Box.setMinimum(30.0)
        self.weight_pp_Box.setMaximum(150.0)
        self.weight_pp_Box.setProperty("value", 80.0)
        self.weight_pp_Box.setObjectName("weight_pp_Box")
        self.formLayout.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.weight_pp_Box)
        self.weight_wc_Label = QtWidgets.QLabel(Dialog)
        self.weight_wc_Label.setObjectName("weight_wc_Label")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.weight_wc_Label)
        self.weight_wc_Box = QtWidgets.QDoubleSpinBox(Dialog)
        self.weight_wc_Box.setMaximum(30.0)
        self.weight_wc_Box.setProperty("value", 10.0)
        self.weight_wc_Box.setObjectName("weight_wc_Box")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.weight_wc_Box)
        self.speed_Label = QtWidgets.QLabel(Dialog)
        self.speed_Label.setObjectName("speed_Label")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.speed_Label)
        self.speed_Box = QtWidgets.QDoubleSpinBox(Dialog)
        self.speed_Box.setMinimum(1.0)
        self.speed_Box.setSingleStep(0.1)
        self.speed_Box.setMaximum(20.0)
        self.speed_Box.setObjectName("speed_Box")
        self.formLayout.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.speed_Box)
        self.power_Label = MyLabel(Dialog)
        self.power_Label.setObjectName("power_Label")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.LabelRole, self.power_Label)
        self.power_Box = QtWidgets.QDoubleSpinBox(Dialog)
        self.power_Box.setMaximum(200.0)
        self.power_Box.setSingleStep(0.1)
        self.power_Box.setProperty("value", 16.0)
        self.power_Box.setObjectName("power_Box")
        self.formLayout.setWidget(3, QtWidgets.QFormLayout.FieldRole, self.power_Box)
        self.power_kg_Label = MyLabel(Dialog)
        self.power_kg_Label.setObjectName("power_kg_Label")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.LabelRole, self.power_kg_Label)
        self.power_kg_Box = QtWidgets.QDoubleSpinBox(Dialog)
        self.power_kg_Box.setMaximum(4.00)
        self.power_kg_Box.setSingleStep(0.01)
        self.power_kg_Box.setProperty("value", 0.20)
        self.power_kg_Box.setDecimals(3)
        self.power_kg_Box.setObjectName("power_kg_Box")
        self.formLayout.setWidget(4, QtWidgets.QFormLayout.FieldRole, self.power_kg_Box)
        self.friction_Label = MyLabel(Dialog)
        self.friction_Label.setObjectName("friction_Label")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.LabelRole, self.friction_Label)
        self.friction_Box = QtWidgets.QDoubleSpinBox(Dialog)
        self.friction_Box.setDecimals(4)
        self.friction_Box.setMaximum(1.0)
        self.friction_Box.setSingleStep(0.001)
        self.friction_Box.setProperty("value", 0.02)
        self.friction_Box.setObjectName("friction_Box")
        self.formLayout.setWidget(5, QtWidgets.QFormLayout.FieldRole, self.friction_Box)
        self.verticalLayout.addLayout(self.formLayout)

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtWidgets.QApplication.translate("Dialog", "Calculate mu", None, -1))
        self.weight_pp_Label.setText(QtWidgets.QApplication.translate("Dialog", "Weight subject [kg]", None, -1))
        self.weight_wc_Label.setText(QtWidgets.QApplication.translate("Dialog", "Weight wheelchair [kg]", None, -1))
        self.speed_Label.setText(QtWidgets.QApplication.translate("Dialog", "Target speed [m/s]", None, -1))
        self.power_Label.setText(QtWidgets.QApplication.translate("Dialog", "Power [W]", None, -1))
        self.power_kg_Label.setText(QtWidgets.QApplication.translate("Dialog", "Power [W/kg]", None, -1))
        self.friction_Label.setText(QtWidgets.QApplication.translate("Dialog", "Friction coefficient", None, -1))


class MyLabel(QtWidgets.QLabel):
    def __init__(self, text):
        super().__init__(text)

    def _set_color(self, col):
        palette = self.palette()
        palette.setColor(self.foregroundRole(), col)
        self.setPalette(palette)

    color = QtCore.Property(QColor, fset=_set_color)


class Form(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowIcon(QtWidgets.QApplication.style().standardIcon(QtWidgets.QStyle.SP_MediaPlay))
        self.animation = []
        self.setupUi(self)
        self.initialize_connections()

    def initialize_connections(self):
        self.weight_pp_Box.valueChanged.connect(self.calculate_stuff)
        self.weight_wc_Box.valueChanged.connect(self.calculate_stuff)
        self.speed_Box.valueChanged.connect(self.calculate_stuff)
        self.friction_Box.valueChanged.connect(lambda: self.calculate_stuff("friction"))
        self.power_kg_Box.valueChanged.connect(lambda: self.calculate_stuff("power_kg"))
        self.power_Box.valueChanged.connect(lambda: self.calculate_stuff("power"))
        self.power_Box.setValue(10.0)
        self.power_kg_Box.setValue(0.2)

    def calculate_stuff(self, source=None):
        weight_pp = self.weight_pp_Box.value()
        weight_wc = self.weight_wc_Box.value()
        speed = self.speed_Box.value()
        power = self.power_Box.value()
        power_kg = self.power_kg_Box.value()
        friction = self.friction_Box.value()

        if source == "friction":
            self.block_signals()
            self.power_Box.setValue((weight_wc+weight_pp) * speed * friction * 9.81)
            self.power_kg_Box.setValue((friction * (weight_wc+weight_pp) * 9.81 * speed)/weight_pp)
            self.unblock_signals()
            self.flash_label(self.power_Label)
            self.flash_label(self.power_kg_Label)
        elif source == "power_kg":
            self.block_signals()
            self.power_Box.setValue(power_kg * weight_pp)
            self.friction_Box.setValue((weight_pp * power_kg) / ((weight_pp+weight_wc) * 9.81 * speed))
            self.unblock_signals()
            self.flash_label(self.friction_Label)
            self.flash_label(self.power_Label)
        else:
            self.block_signals()
            self.power_kg_Box.setValue(power / weight_pp)
            self.friction_Box.setValue(power / ((weight_pp+weight_wc) * 9.81 * speed))
            self.unblock_signals()
            self.flash_label(self.friction_Label)
            self.flash_label(self.power_kg_Label)

    def block_signals(self):
        self.friction_Box.blockSignals(True)
        self.power_kg_Box.blockSignals(True)
        self.power_Box.blockSignals(True)

    def unblock_signals(self):
        self.friction_Box.blockSignals(False)
        self.power_kg_Box.blockSignals(False)
        self.power_Box.blockSignals(False)

    def flash_label(self, label):
        anim = QtCore.QPropertyAnimation(label, b"color")
        self.animation.append(anim)
        anim.setDuration(400)
        anim.setLoopCount(2)
        anim.setStartValue(QColor(0, 0, 0))
        anim.setEndValue(QColor(0, 0, 0))
        anim.setKeyValueAt(0.5, QColor(255, 255, 255))
        anim.start()


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)  # Create the Qt Application
    form = Form()  # Create and show the form
    form.show()
    sys.exit(app.exec_())  # Run the main Qt loop
